/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef CHARACTERWIDGET_H
#define CHARACTERWIDGET_H

#include <QLabel>
#include <QMovie>
#include <QFile>
#include <QDir>
#include <QResource>
#include <QTimerEvent>
#include <QPainter>
#include <QRegion>
#include <QBitmap>

#include "exercise.h"

class CharacterWidget : public QLabel
{
    Q_OBJECT
public:
    enum Stage{NoAnimation, Introduction, Process, Conclusion};
protected:
    Stage currentStage;

    QFile *beforeAnimation;
    QFile *animation;
    QFile *afterAnimation;

    QMovie *movie;

    Exercise *exercise;

    void showIntroduction();
    void showProcess();
    void showConclusion();

    void paintEvent(QPaintEvent *);
public:
    explicit CharacterWidget(QWidget *parent = 0);
    ~CharacterWidget();

    void setExercise(Exercise *exercise);
    void clearExercise();
    Stage getCurrentStage() const;


public slots:
    void startAnimation(bool intro = true);
    void stopAnimation();
signals:
    void finished();
private slots:
    void fileFinished();
    void movieFrameChanged(int);
};

#endif // CHARACTERWIDGET_H
