/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef EXERCICEDIALOG_H
#define EXERCICEDIALOG_H

#include <QDebug>
#include <QWidget>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>
#include <QBitmap>
#include <QGridLayout>
#include <QIcon>
#include <QPushButton>
#include <QToolButton>
#include <QLabel>

#include "characterwidget.h"
#include "exercise.h"

class ExerciseDialog : public QWidget
{
    Q_OBJECT
protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);
private:
    CharacterWidget *character;
    QLabel *exerciseCaption;
    QLabel *exerciseText;
    QPushButton *nextExerciseBtn;
    QToolButton *closeBtn;
    Exercise *currExercise;
    QWidget *blockW;
public:
    explicit ExerciseDialog(QWidget *parent = 0);
    ~ExerciseDialog();

    CharacterWidget *getCharacterWidget() const;

signals:
    void manuallyClosed();
public slots:
    void show(uint flags, bool showExercises = false);
    void hide();
    void canHide();
    void nextExercise();
    
};

#endif // EXERCICEDIALOG_H
