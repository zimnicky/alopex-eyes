/*
*   Copyright (c) 2013 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Timer.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "aboutdialog.h"
#include "ui_aboutdialog.h"
#include <QPlainTextEdit>
#include <QMessageBox>
#include "settings.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
    setMaximumSize(size());
    setMinimumSize(size());

    licenseDialog = new QDialog(this);
    licenseDialog->setWindowTitle(tr("License"));
    licenseDialog->setMinimumSize(400,300);
    QGridLayout *layout = new QGridLayout(licenseDialog);
    QPlainTextEdit *edit = new QPlainTextEdit(Settings::instance()->license());
    edit->setReadOnly(true);
    layout->addWidget(edit,0,0,1,2);
    QPushButton *button = new QPushButton(tr("Close"));
    connect(button, SIGNAL(clicked()), licenseDialog, SLOT(accept()));
    button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    layout->addWidget(button,1,1,1,1);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange)
        ui->retranslateUi(this);
}

void AboutDialog::on_pushButton_2_clicked()
{
    licenseDialog->exec();
}
