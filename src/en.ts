<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="26"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="133"/>
        <source>Tetyana Kolodyazhna (leneron@ukr.net)</source>
        <translation>Татьяна Колодяжная (leneron@ukr.net)</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="218"/>
        <source>Alopex Eyes 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="174"/>
        <source>Arctic fox cares on you...</source>
        <oldsource>Arctic fox care on you...</oldsource>
        <translation>Arctic fox cares on you...</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="254"/>
        <source>Autors:</source>
        <translation>Autors:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="152"/>
        <location filename="aboutdialog.cpp" line="29"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="72"/>
        <source>Development:</source>
        <translation>Development:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="94"/>
        <source>Pawel Zimnicki (zimnicky@gmail.com)</source>
        <oldsource>Pawel Zimnicki(zimnicky@gmail.com)</oldsource>
        <translation>Pawel Zimnicki (zimnicky@gmail.com)</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="117"/>
        <source>Character design:</source>
        <translation>Character design:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="193"/>
        <location filename="aboutdialog.cpp" line="35"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>ExerciseDialog</name>
    <message>
        <location filename="exercisedialog.cpp" line="147"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="exercisedialog.cpp" line="153"/>
        <source>It&apos;s time for break.</source>
        <translation>It&apos;s time for break.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="mainclass.cpp" line="86"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="mainclass.cpp" line="92"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="mainclass.cpp" line="98"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="76"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="98"/>
        <source>Automatically start program at startup</source>
        <translation>Automatically start program at startup</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="114"/>
        <source>Language:</source>
        <translation>Language:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="130"/>
        <source>Timers</source>
        <translation>Timers</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="151"/>
        <source>Short break</source>
        <translation>Short break</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="338"/>
        <location filename="settingsdialog.ui" line="535"/>
        <source>Show close button</source>
        <translation>Show close button</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="348"/>
        <source>Long break</source>
        <translation>Long break</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="199"/>
        <location filename="settingsdialog.ui" line="396"/>
        <source>Interval:</source>
        <translation>Interval:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="234"/>
        <location filename="settingsdialog.ui" line="318"/>
        <location filename="settingsdialog.ui" line="431"/>
        <location filename="settingsdialog.ui" line="515"/>
        <source>HH:mm:ss</source>
        <translation>HH:mm:ss</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="277"/>
        <location filename="settingsdialog.ui" line="474"/>
        <source>Duration:</source>
        <translation>Duration:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="331"/>
        <location filename="settingsdialog.ui" line="528"/>
        <source>Block all programms</source>
        <translation>Block all programms</translation>
    </message>
</context>
</TS>
