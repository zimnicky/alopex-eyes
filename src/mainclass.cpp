/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "main.h"
#include "exercise.h"

Main::Main(QObject *parent) :
    QObject(parent)
{
    shortTimer = new ShortTimer();
    longTimer = new LongTimer();
    settings = Settings::instance();
    settingsDialog = nullptr;
    aboutDialog = nullptr;

    createMenu();
}

Main::~Main()
{
    if (settingsDialog != nullptr) delete settingsDialog;
    if (aboutDialog != nullptr) delete aboutDialog;
    delete longTimer;
    delete shortTimer;
}

void Main::start()
{
    longTimer->stop();
    shortTimer->stop();
    loadSettings();
    if (longTimerEnabled) longTimer->start();
    if (shortTimerEnabled)
    {
        connect(longTimer, SIGNAL(actionStarted()), shortTimer, SLOT(stop()));
        connect(longTimer, SIGNAL(actionFinished()), shortTimer, SLOT(start()));
        shortTimer->start();
    }
}

void Main::loadSettings()
{
    longTimer->setInterval(settings->longTimerInterval());
    longTimer->setActionInterval(settings->longTimerActionInterval());
    longTimer->setFlags(settings->longTimerFlags());

    shortTimer->setInterval(settings->shortTimerInterval());
    shortTimer->setActionInterval(settings->shortTimerActionInterval());
    shortTimer->setFlags(settings->shortTimerFlags());

    longTimerEnabled = settings->longTimerEnabled();
    shortTimerEnabled = settings->shortTimerEnabled();

    Exercise::setCharacter(Settings::instance()->character());
}

void Main::showSettingsDialog()
{
    if (settingsDialog == nullptr)
        settingsDialog = new SettingsDialog();

    if (settingsDialog->exec() == QDialog::Accepted)
    {
        start();
        retranslateMenu();
    }
}

void Main::showAboutDialog()
{
    if (aboutDialog == nullptr)
        aboutDialog = new AboutDialog();

    aboutDialog->exec();
}

void Main::createMenu()
{
    trayIcon = new QSystemTrayIcon(QIcon(":/images/icon.png"));
    QMenu *menu = new QMenu();

    QString str = tr("Settings");
    QAction *action = new QAction(str, menu);
    action->setData("Settings");
    connect(action, SIGNAL(triggered()), SLOT(showSettingsDialog()));
    menu->addAction(action);

    str = tr("About");
    action = new QAction(str, menu);
    action->setData("About");
    connect(action, SIGNAL(triggered()), SLOT(showAboutDialog()));
    menu->addAction(action);

    str = tr("Quit");
    action = new QAction(str, menu);
    action->setData("Quit");
    connect(action, SIGNAL(triggered()), qApp, SLOT(quit()));
    menu->addAction(action);

    trayIcon->setContextMenu(menu);
    trayIcon->show();
}


void Main::retranslateMenu()
{
    QList<QAction*> actions = trayIcon->contextMenu()->actions();
    for (auto &i: actions)
        i->setText(tr((char*)(i->data().toByteArray().constData())));
}
