/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "exercisedialog.h"
#include "timer.h"

ExerciseDialog::ExerciseDialog(QWidget *parent) :
    QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint |
                   Qt::X11BypassWindowManagerHint);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setMaximumWidth(400);
    setMinimumWidth(400);
    setWindowIcon(QIcon(":/icons/main"));

    blockW = new QWidget(0);
    blockW->setWindowFlags(Qt::FramelessWindowHint  | Qt::WindowStaysOnTopHint |
                      Qt::X11BypassWindowManagerHint);
    blockW->setStyleSheet("background: black;");
    blockW->setGeometry(0, 0, qApp->desktop()->screenGeometry().width(),
                qApp->desktop()->screenGeometry().height());
    blockW->setVisible(false);
    blockW->setWindowOpacity(0.5);

    character = new CharacterWidget(this);
    character->setGeometry(0, 0, 300, 380);
    character->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    character->setScaledContents(false);
    connect(character, SIGNAL(finished()), SLOT(hide()));

    exerciseCaption = new QLabel(this);
    QFont font;
    font.setBold(true);
    font.setFamily("Trebuchet MS");
    font.setPointSize(14);
    exerciseCaption->setFont(font);
    exerciseCaption->setWordWrap(true);
    exerciseCaption->setStyleSheet("color: #ee9506; background: white;");
    exerciseCaption->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    exerciseText = new QLabel(this);
    font.setBold(false);
    font.setFamily("Arial");
    font.setPointSize(9);
    exerciseText->setFont(font);
    exerciseText->setWordWrap(true);
    exerciseText->setStyleSheet("color: black; background: white;");
    exerciseText->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    exerciseText->setContentsMargins(0, 8, 0, 0);

    nextExerciseBtn = new QPushButton(this);
    nextExerciseBtn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    nextExerciseBtn->setAutoDefault(false);
    nextExerciseBtn->setDefault(false);
    nextExerciseBtn->setFlat(false);
    font.setPointSize(11);
    connect(nextExerciseBtn, SIGNAL(clicked()), SLOT(nextExercise()));

    closeBtn = new QToolButton(this);
    nextExerciseBtn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    closeBtn->setGeometry(this->width()-40,5,32,32);
    closeBtn->setToolButtonStyle(Qt::ToolButtonTextOnly);
    closeBtn->setStyleSheet(
                "QToolButton{background: url(:/images/close.png); border: 0; padding: 0; margin: 0;}"
                "QToolButton:hover{background: url(:/images/close_hover.png);}"
                "QToolButton:pressed{border:1px solid silver; border-radius: 2px;}");
    connect(closeBtn, SIGNAL(clicked()), SIGNAL(manuallyClosed()));

    QGridLayout *layout = new QGridLayout(this);
    layout->setSpacing(0);
    layout->setContentsMargins(20, 6, 20, 15);
    layout->addWidget(character, 0, 0, 1, 3, Qt::AlignCenter);
    layout->addWidget(exerciseCaption, 1, 0, 1, 2);
    layout->addWidget(exerciseText, 2, 0, 1, 3, Qt::AlignTop);
    layout->addWidget(nextExerciseBtn, 1, 2);

    setLayout(layout);

    currExercise = nullptr;
    connect(this, SIGNAL(manuallyClosed()), SLOT(hide()));
}

ExerciseDialog::~ExerciseDialog()
{
    delete blockW;
    delete character;
    delete exerciseCaption;
    delete exerciseText;
    if (currExercise != nullptr)
        delete currExercise;
}

void ExerciseDialog::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBackground(Qt::white);
    painter.setBrush(Qt::white);
    QPen pen(QColor(255, 170, 0));
    pen.setWidth(6);
    painter.setPen(pen);
    painter.drawRoundedRect(0,0, width(), height(), 20, 20);
}

void ExerciseDialog::resizeEvent(QResizeEvent *)
{
    int x = QApplication::desktop()->screenGeometry().width()/2 - width()/2;
    int y = QApplication::desktop()->screenGeometry().height()/2 - height()/2;
    move(x, y);
    QBitmap bitmap(width(), height());
    bitmap.fill();
    QPainter painter;
    painter.begin(&bitmap);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBackground(Qt::white);
    painter.setPen(Qt::black);
    painter.setBrush(Qt::black);
    painter.drawRoundedRect(0,0,width(), height(), 20, 20);
    painter.end();
    setMask(bitmap);
}

void ExerciseDialog::show(uint flags, bool showexercises)
{
    closeBtn->setVisible(flags & Timer::Flag::showCloseButton);
    blockW->setVisible(flags & Timer::Flag::blockWork);
    exerciseText->setVisible(showexercises);
    if (showexercises)
    {
       currExercise = new Exercise(rand()%Exercise::countExercises());
       character->setExercise(currExercise);
       exerciseCaption->setText(currExercise->getCaption());
       exerciseText->setText(currExercise->getText());
       character->startAnimation();
       nextExerciseBtn->setText(tr("Next"));
       nextExerciseBtn->setVisible(true);
    }
    else
    {
        character->clearExercise();
        exerciseCaption->setText(tr("It's time for break."));
         nextExerciseBtn->setVisible(false);
    }
    QWidget::show();
}


void ExerciseDialog::hide()
{
    blockW->setVisible(false);
    if (currExercise != nullptr)
        delete currExercise;
    QWidget::hide();
}

void ExerciseDialog::canHide()
{
    character->stopAnimation();
}


CharacterWidget * ExerciseDialog::getCharacterWidget() const
{
    return character;
}


void ExerciseDialog::nextExercise()
{
    Exercise *t = new Exercise((currExercise->getNumber()+1)%Exercise::countExercises());
    character->setExercise(t);
    character->startAnimation(false);
    exerciseCaption->setText(t->getCaption());
    exerciseText->setText(t->getText());
    delete currExercise;
    currExercise = t;
    this->adjustSize();
}
