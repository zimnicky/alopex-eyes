/*
*   Copyright (c) 2013 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Timer.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef SETTINGS_H
#define SETTINGS_H

#include <QTime>
#include <QSettings>
#include <QStringList>
#include <QLocale>
#include <QTranslator>
#include <QDir>


class Settings
{
protected:
    QSettings settings;

    Settings();
    ~Settings();
    Settings(const Settings&);
    Settings& operator=(const Settings&);

    QDir lang_dir;
    QDir program_dir;
    QTranslator translator;
public:
    static Settings* instance();
    QStringList getLanguages() const;
    void language(const QString &);
    QString language() const;
    QString locale() const;
    QString character() const;

    void autorun(bool on);
    bool autorun() const;

    int longTimerInterval() const;
    void longTimerInterval(int);

    int longTimerActionInterval() const;
    void longTimerActionInterval(int);

    int shortTimerInterval() const;
    void shortTimerInterval(int);

    int shortTimerActionInterval() const;
    void shortTimerActionInterval(int);

    uint longTimerFlags() const;
    void longTimerFlags(uint);

    uint shortTimerFlags() const;
    void shortTimerFlags(uint);

    bool longTimerEnabled() const;
    void longTimerEnabled(bool);

    bool shortTimerEnabled() const;
    void shortTimerEnabled(bool);

    QString license() const;
};

#endif // SETTINGS_H
