/*
*   Copyright (c) 2013 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Timer.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "settings.h"
#include <QApplication>
#include <QDebug>
#include "timer.h"

Settings::Settings()
{
    program_dir = QDir::current();
    #ifndef QT_DEBUG
    #ifdef Q_OS_UNIX
        program_dir = QDir("/usr/share/alopex-eyes");
    #endif
    #endif

    if (settings.contains("lang_dir"))
        lang_dir = settings.value("lang_dir").toString();
    else
    {
        lang_dir = program_dir;
        lang_dir.cd("lang");
        settings.setValue("lang_dir", lang_dir.absolutePath());
    }

    if (!settings.contains("character"))
        settings.setValue("character", program_dir.absoluteFilePath("characters/default"));

    QString lang = settings.value("language", QLocale::system().name()).toString();

    translator.load(lang, lang_dir.absolutePath());
    QLocale::setDefault(QLocale(lang));

    qApp->installTranslator(&translator);
}

Settings::~Settings(){}

Settings* Settings::instance()
{
    static Settings i;
    return &i;
}

QStringList Settings::getLanguages() const
{
    QFileInfoList languages = QDir(lang_dir).entryInfoList(QStringList("*.qm"));
    QStringList res;
    for (auto i = languages.begin(); i != languages.end(); i++)
    {
        QLocale locale(i->baseName());
        res.append(locale.nativeLanguageName());
    }
    return res;
}

void Settings::language(const QString &l)
{
    QFileInfoList languages = lang_dir.entryInfoList(QStringList("*.qm"));
    for (auto i = languages.begin(); i != languages.end(); i++)
    {
        QLocale locale(i->baseName());
        if (l.compare(locale.nativeLanguageName(), Qt::CaseInsensitive) == 0)
        {
            settings.setValue("language", locale.name());
            translator.load(locale.name(), lang_dir.absolutePath());
            QLocale::setDefault(locale);
        }
    }

}

QString Settings::language() const
{
    QString lang = settings.value("language", QLocale::system().name()).toString();
    return QLocale(lang).nativeLanguageName();
}

QString Settings::locale() const
{
    QString lang = settings.value("language",QLocale::system().name()).toString();
    return lang;
}

QString Settings::character() const
{
    return settings.value("character").toString();
}

void Settings::autorun(bool on)
{
    settings.setValue("autorun", on);
    #ifdef Q_OS_WIN32
        QSettings *a = new QSettings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",QSettings::NativeFormat);
        if(on)
        {
            a->setValue(QApplication::applicationName(), QDir::toNativeSeparators(QCoreApplication::applicationFilePath()));
            a->sync();
        }
        else
        {
            a->remove(QApplication::applicationName());
        }
        delete a;
    #endif
}


bool Settings::autorun() const
{
    return settings.value("autorun", false).toBool();
}

int Settings::longTimerInterval() const
{
    return settings.value("longTimer/interval", 3000000).toInt();
}

void Settings::longTimerInterval(int i)
{
    settings.setValue("longTimer/interval", i);
}

int Settings::longTimerActionInterval() const
{
    return settings.value("longTimer/actionInterval", 300000).toInt();
}

void Settings::longTimerActionInterval(int i)
{
    settings.setValue("longTimer/actionInterval", i);
}

int Settings::shortTimerInterval() const
{
    return settings.value("shortTimer/interval", 900000).toInt();
}

void Settings::shortTimerInterval(int i)
{
    settings.setValue("shortTimer/interval", i);
}

int Settings::shortTimerActionInterval() const
{
    return settings.value("shortTimer/actionInterval", 30000).toInt();
}

void Settings::shortTimerActionInterval(int i)
{
    settings.setValue("shortTimer/actionInterval", i);
}

uint Settings::longTimerFlags() const
{
    return settings.value("longTimer/flags", Timer::blockWork).toUInt();
}

void Settings::longTimerFlags(uint f)
{
    settings.setValue("longTimer/flags", f);
}

uint Settings::shortTimerFlags() const
{
    return settings.value("shortTimer/flags", 0).toUInt();
}

void Settings::shortTimerFlags(uint f)
{
    settings.setValue("shortTimer/flags", f);
}

bool Settings::longTimerEnabled() const
{
    return settings.value("longTimer/enabled", true).toBool();
}

void Settings::longTimerEnabled(bool val)
{
    settings.setValue("longTimer/enabled", val);
}

bool Settings::shortTimerEnabled() const
{
    return settings.value("shortTimer/enabled", true).toBool();
}

void Settings::shortTimerEnabled(bool val)
{
    settings.setValue("shortTimer/enabled", val);
}


QString Settings::license() const
{
    if (!program_dir.exists("license.txt"))
        return "License file not found.";
    QFile f(program_dir.absoluteFilePath("license.txt"));
    f.open(QFile::ReadOnly);
    QString r = f.readAll();
    f.close();
    return r;
}
