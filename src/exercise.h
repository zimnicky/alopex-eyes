/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef EXERCICE_H
#define EXERCICE_H

#include <QString>
#include <QDir>
#include <QXmlStreamReader>

class Exercise
{
protected:
    uint num;

    QString name; // needs only for transalations, optional

    QString caption;
    QString text;
    QString beforeAnimation;
    QString animation;
    QString afterAnimation;

    static QDir character;

    void readExercise(QXmlStreamReader &xml);
    void readTranslation(QXmlStreamReader &xml);
    static QString getData(QXmlStreamReader &xml);
public:
    Exercise(uint num);
    ~Exercise(){}

    static uint countExercises();

    static void setCharacter(const QString &ch);
    static QString noExerciseImage();

    uint getNumber() const {return num;}
    QString getCaption() const {return caption;}
    QString getText() const {return text;}
    QString getBeforeAnimationFilename() const {return beforeAnimation;}
    QString getAnimationFilename() const {return animation;}
    QString getAfterAnimationFilename() const {return afterAnimation;}

};

#endif // EXERCICE_H
