/*
*   Copyright (c) 2013 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Timer.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "timer.h"


SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    setMaximumSize(size());
    setMinimumSize(size());
    #ifndef Q_OS_WIN32
        ui->autorun->setEnabled(false);
    #endif
    settings = Settings::instance();
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

int SettingsDialog::exec()
{
    ui->autorun->setChecked(settings->autorun());

    int i = 0;
    ui->language->clear();
    ui->language->addItems(settings->getLanguages());
    QString lang = settings->language();
    while (i < ui->language->count() && ui->language->itemText(i) != lang)
        i++;
    if (i < ui->language->count())
        ui->language->setCurrentIndex(i);

    ui->gbLong->setChecked(settings->longTimerEnabled());
    ui->gbShort->setChecked(settings->shortTimerEnabled());

    ui->shortInterval->setTime(QTime::fromMSecsSinceStartOfDay(settings->shortTimerInterval()));
    ui->shortDuration->setTime(QTime::fromMSecsSinceStartOfDay(settings->shortTimerActionInterval()));
    uint flags = settings->shortTimerFlags();
    ui->shortBlockAll->setChecked(flags & Timer::blockWork);
    ui->shortClose->setChecked(flags & Timer::showCloseButton);

    ui->longInterval->setTime(QTime::fromMSecsSinceStartOfDay(settings->longTimerInterval()));
    ui->longDuration->setTime(QTime::fromMSecsSinceStartOfDay(settings->longTimerActionInterval()));
    flags = settings->longTimerFlags();
    ui->longBlockAll->setChecked((flags & Timer::blockWork)!=0);
    ui->longClose->setChecked((flags & Timer::showCloseButton)!=0);

    return QDialog::exec();
}

void SettingsDialog::accept()
{
    settings->autorun(ui->autorun->isChecked());

    settings->longTimerEnabled(ui->gbLong->isChecked());
    settings->shortTimerEnabled(ui->gbShort->isChecked());

    settings->shortTimerInterval(ui->shortInterval->time().msecsSinceStartOfDay());
    settings->shortTimerActionInterval(ui->shortDuration->time().msecsSinceStartOfDay());
    uint flags = 0;
    if (ui->shortBlockAll->isChecked()) flags = Timer::blockWork;
    if (ui->shortClose->isChecked()) flags |= Timer::showCloseButton;
    settings->shortTimerFlags(flags);

    settings->longTimerInterval(ui->longInterval->time().msecsSinceStartOfDay());
    settings->longTimerActionInterval(ui->longDuration->time().msecsSinceStartOfDay());
    flags = 0;
    if (ui->longBlockAll->isChecked()) flags = Timer::blockWork;
    if (ui->longClose->isChecked()) flags |= Timer::showCloseButton;
    settings->longTimerFlags(flags);


    QDialog::accept();
}

void SettingsDialog::changeEvent(QEvent *e)
{
    if (e->type() == QEvent::LanguageChange)
        ui->retranslateUi(this);
}

void SettingsDialog::on_language_currentIndexChanged(const QString &arg1)
{
    if (ui->language->isActiveWindow())
        settings->language(arg1);
}
