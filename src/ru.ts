<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru" sourcelanguage="en">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="26"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="133"/>
        <source>Tetyana Kolodyazhna (leneron@ukr.net)</source>
        <translation>Татьяна Колодяжная (leneron@ukr.net)</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="218"/>
        <source>Alopex Eyes 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="174"/>
        <source>Arctic fox cares on you...</source>
        <oldsource>Arctic fox care on you...</oldsource>
        <translation>Полярный лис заботится о тебе...</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="254"/>
        <source>Autors:</source>
        <translation>Создатели:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="152"/>
        <location filename="aboutdialog.cpp" line="29"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="72"/>
        <source>Development:</source>
        <translation>Разработка:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="94"/>
        <source>Pawel Zimnicki (zimnicky@gmail.com)</source>
        <oldsource>Pawel Zimnicki(zimnicky@gmail.com)</oldsource>
        <translation>Павел Зимницкий (zimnicky@gmail.com)</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="117"/>
        <source>Character design:</source>
        <translation>Дизайн персонажей:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Версия:</translation>
    </message>
    <message>
        <location filename="aboutdialog.ui" line="193"/>
        <location filename="aboutdialog.cpp" line="35"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>ExerciseDialog</name>
    <message>
        <location filename="exercisedialog.cpp" line="147"/>
        <source>Next</source>
        <translation>Следующее</translation>
    </message>
    <message>
        <location filename="exercisedialog.cpp" line="153"/>
        <source>It&apos;s time for break.</source>
        <translation>Время отдохнуть.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="mainclass.cpp" line="86"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainclass.cpp" line="92"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainclass.cpp" line="98"/>
        <source>Quit</source>
        <translation>Завершить</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="76"/>
        <source>General</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="98"/>
        <source>Automatically start program at startup</source>
        <translation>Запускать при старте системы</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="114"/>
        <source>Language:</source>
        <translation>Язык:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="130"/>
        <source>Timers</source>
        <translation>Таймеры</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="151"/>
        <source>Short break</source>
        <translation>Короткий перерыв</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="338"/>
        <location filename="settingsdialog.ui" line="535"/>
        <source>Show close button</source>
        <translation>Возможность закрыть</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="348"/>
        <source>Long break</source>
        <translation>Длинный перерыв</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="199"/>
        <location filename="settingsdialog.ui" line="396"/>
        <source>Interval:</source>
        <translation>Интервал:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="234"/>
        <location filename="settingsdialog.ui" line="318"/>
        <location filename="settingsdialog.ui" line="431"/>
        <location filename="settingsdialog.ui" line="515"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="277"/>
        <location filename="settingsdialog.ui" line="474"/>
        <source>Duration:</source>
        <translation>Время:</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="331"/>
        <location filename="settingsdialog.ui" line="528"/>
        <source>Block all programms</source>
        <translation>Блокировать работу</translation>
    </message>
</context>
</TS>
