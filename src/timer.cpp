/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "timer.h"

#include <QTimerEvent>
#include <QDebug>

Timer::Timer(int i, int ai): QObject(), timerInterval(i),
    actionInterval(ai),actionFlags(0), started(false)
{
    timerId = 0;
    defaultIntervalIteration = 500;

    dialog = new ExerciseDialog();

    connect(dialog, SIGNAL(manuallyClosed()), SLOT(restart()));
    connect(dialog, SIGNAL(manuallyClosed()), SIGNAL(actionFinished()));
}

Timer::~Timer()
{
    delete dialog;
}

void Timer::setInterval(int t)
{
    if (started){
        stop();
        timerInterval = t;
        start();
    } else timerInterval = t;
}

void Timer::setActionInterval(int t)
{
    if (started){
        stop();
        actionInterval = t;
        start();
    } else actionInterval = t;
}

void Timer::start()
{
    if (!started && timerInterval > 0){
        stage = Stage::Timer;
        started = true;
        elapsed = 0;
        timerId = startTimer(defaultIntervalIteration);
        emit timerStarted();
    }
}

void Timer::stop()
{
    started = false;
    if (timerId > 0) killTimer(timerId);
    timerId = 0;
    if (dialog->isVisible()) dialog->hide();
    emit timerStopped();
}

void Timer::restart()
{
    stop();
    start();
}

void Timer::timerEvent(QTimerEvent *)
{
    if (stage == Stage::Timer)
    {
        elapsed += defaultIntervalIteration;
        if (elapsed >= timerInterval)
        {
            stage = Stage::Action;
            elapsed = 0;
            action();
            emit actionStarted();
        }
    }
    else
    {
        if (!dialog->isVisible())
        {
            stage = Stage::Timer;
            elapsed = 0;
            emit actionFinished();
        }
        else if (dialog->getCharacterWidget()->getCurrentStage() == CharacterWidget::Process
                 || dialog->getCharacterWidget()->getCurrentStage() == CharacterWidget::NoAnimation)
        {
            elapsed += defaultIntervalIteration;
            if (elapsed >= actionInterval)
                dialog->canHide();
        }
    }
}

