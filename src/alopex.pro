QT += core gui widgets

QMAKE_CXXFLAGS += -std=c++11

RESOURCES += \
    images.qrc

FORMS += \
    aboutdialog.ui \
    settingsdialog.ui

HEADERS += \
    aboutdialog.h \
    characterwidget.h \
    exercise.h \
    exercisedialog.h \
    longtimer.h \
    main.h \
    settings.h \
    settingsdialog.h \
    shorttimer.h \
    timer.h

SOURCES += \
    aboutdialog.cpp \
    characterwidget.cpp \
    exercise.cpp \
    exercisedialog.cpp \
    longtimer.cpp \
    main.cpp \
    mainclass.cpp \
    settings.cpp \
    settingsdialog.cpp \
    shorttimer.cpp \
    timer.cpp

TRANSLATIONS += ru.ts \
                en.ts \
                uk.ts

win32 {
    RC_FILE = alopex.rc
}
