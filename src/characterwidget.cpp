/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "characterwidget.h"
#include <QDebug>


CharacterWidget::CharacterWidget(QWidget *parent) :
    QLabel(parent)
{
    setWindowFlags(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);
    exercise = nullptr;
    beforeAnimation = 0;
    animation = 0;
    afterAnimation = 0;
    movie = new QMovie(this);
    movie->setCacheMode(QMovie::CacheAll);
    connect(movie, SIGNAL(finished()), this, SLOT(fileFinished()));
    connect(movie, SIGNAL(frameChanged(int)), SLOT(movieFrameChanged(int)));
    setPixmap(QPixmap(Exercise::noExerciseImage()));
}

CharacterWidget::~CharacterWidget()
{
    clearExercise();
    delete movie;
}


void CharacterWidget::setExercise(Exercise *exercise)
{
    clearExercise();
    this->exercise = exercise;

    QFile file;
    file.setFileName(exercise->getAnimationFilename());
    if (file.exists())
    {
        animation = new QFile(file.fileName());
        animation->open(QFile::ReadOnly);
    }
    file.setFileName(exercise->getBeforeAnimationFilename());
    if (file.exists())
    {
        beforeAnimation = new QFile(file.fileName());
        beforeAnimation->open(QFile::ReadOnly);
    }
    file.setFileName(exercise->getAfterAnimationFilename());
    if (file.exists())
    {
        afterAnimation = new QFile(file.fileName());
        afterAnimation->open(QFile::ReadOnly);
    }
}

void CharacterWidget::clearExercise()
{
    if (beforeAnimation != 0)
    {
        delete beforeAnimation;
        beforeAnimation = 0;
    }
    if (animation != 0)
    {
        delete animation;
        animation = 0;
    }
    if (afterAnimation != 0)
    {
        delete afterAnimation;
        afterAnimation = 0;
    }
    exercise = nullptr;
    clear();
    setPixmap(QPixmap(Exercise::noExerciseImage()));
    currentStage = NoAnimation;
}

void CharacterWidget::showIntroduction()
{
    movie->stop();
    if (beforeAnimation != 0)
    {
        currentStage = Introduction;
        movie->setDevice(beforeAnimation);
        movie->start();
    }
    else showProcess();
}

void CharacterWidget::showProcess()
{
    movie->stop();
    currentStage = Process;
    if (animation != 0)
    {
        movie->setDevice(animation);
        movie->start();
    }
}

void CharacterWidget::showConclusion()
{
    movie->stop();
    if (afterAnimation != 0)
    {
        currentStage = Conclusion;
        movie->setDevice(afterAnimation);
        movie->start();
    }
    else emit finished();
}

void CharacterWidget::fileFinished()
{
    switch (currentStage) {
        case Introduction:
    {
        showProcess();
    }break;
        case Process: movie->start(); break;
    case Conclusion: {
        emit finished(); break;
    }
        default: break;
    }
}


void CharacterWidget::movieFrameChanged(int frame)
{
    if (movie->loopCount() == -1 && frame == movie->frameCount() - 1)
    {
        movie->stop();
        fileFinished();
    }
}

void CharacterWidget::paintEvent(QPaintEvent *e)
{
    QLabel::paintEvent(e);
    //transparent background:
    /*QPixmap *pixmap = new QPixmap(movie->currentPixmap());
    QPainter painter(this);
    painter.setBackgroundMode(Qt::TransparentMode);
    QBitmap bitmap = pixmap->createHeuristicMask();
    QRegion region(bitmap);
    painter.setClipRegion(region);
    painter.drawPixmap(0,0, *pixmap);*/
}



void CharacterWidget::startAnimation(bool intro)
{
    if (exercise != nullptr)
    {
        clear();
        setMovie(movie);
        if (intro)
            showIntroduction();
        else
            showProcess();
    }
}

void CharacterWidget::stopAnimation()
{
    if (exercise != nullptr)
        showConclusion();
    else
        emit finished();
}


CharacterWidget::Stage CharacterWidget::getCurrentStage() const
{
    return currentStage;
}
