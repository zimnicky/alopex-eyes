/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include "exercisedialog.h"

class Timer: public QObject
{
    Q_OBJECT
protected:
    enum class Stage{Timer, Action};
    int defaultIntervalIteration;

    int timerInterval;
    int actionInterval;

    int elapsed;

    uint actionFlags;
    ExerciseDialog *dialog;

    bool started;
    int timerId;
    Stage stage;

    void timerEvent(QTimerEvent *);
    virtual void action() = 0;
public:
    enum Flag{showCloseButton = 1, blockWork = 2};

    Timer(int i = 0, int ai = 0);
    virtual ~Timer();

    void setInterval(int t);
    void setActionInterval(int t);

    void setFlags(uint flag){actionFlags = flag;}

    bool isStarted() const {return started;}
    int getInterval() const {return timerInterval;}

    uint getFlags() const {return actionFlags;}

public slots:
    virtual void start();
    virtual void stop();
    void restart();

signals:
    void timerStarted();
    void timerStopped();
    void actionStarted();
    void actionFinished();
};

#endif // TIMER_H
