/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#include "exercise.h"

#include <QFile>
#include <settings.h>
#include <QDebug>

QDir Exercise::character;

Exercise::Exercise(uint num)
{
    this->num = num;
    QFile info(Exercise::character.absoluteFilePath("info.xml"));

    if (!info.open(QFile::ReadOnly | QIODevice::Text)) return;

    QXmlStreamReader xml(&info);
    uint i = 0;
    while (!xml.atEnd() && i <= num)
    {
        xml.readNext();
        if (xml.tokenType() == QXmlStreamReader::StartElement && xml.name() == "exercise")
            i++;
    }

    if (!xml.hasError()) readExercise(xml);
    else qDebug() << xml.errorString();

    xml.clear();

   if (character.cd("translations"))
   {
       QFileInfoList list = character.entryInfoList();
       for (auto it = list.begin(); it != list.end(); it++)
           if (QLocale(it->baseName()).nativeLanguageName() == Settings::instance()->language())
           {
               QFile f(it->absoluteFilePath());
               if (f.open(QFile::ReadOnly | QIODevice::Text))
               {
                   xml.setDevice(&f);
                   readTranslation(xml);
                   xml.clear();
                   f.close();
               }
           }
       character.cdUp();
   }
}


QString Exercise::getData(QXmlStreamReader &xml)
{
    if (xml.tokenType() != QXmlStreamReader::StartElement) return "";

    xml.readNext();
    return xml.text().toString();
}

void Exercise::readExercise(QXmlStreamReader &xml)
{
   if (xml.attributes().hasAttribute("name"))
       name = xml.attributes().value("name").toString();
   QXmlStreamReader::TokenType token = xml.readNext();
   while (!xml.atEnd() &&
          !(token == QXmlStreamReader::EndElement && xml.name() == "exercise"))
   {
       if (token == QXmlStreamReader::StartElement)
       {
           if (xml.name() == "caption") caption = getData(xml);
           else if (xml.name() == "text") text = getData(xml);
           else if (xml.name() == "animation")
               animation = Exercise::character.absoluteFilePath(getData(xml));
           else if (xml.name() == "before")
               beforeAnimation = Exercise::character.absoluteFilePath(getData(xml));
           else if (xml.name() == "after")
               afterAnimation = Exercise::character.absoluteFilePath(getData(xml));
       }
       token = xml.readNext();
   }
}

void Exercise::readTranslation(QXmlStreamReader &xml)
{
    QXmlStreamReader::TokenType token = xml.readNext();
    while (!xml.atEnd())
    {
        if (token == QXmlStreamReader::StartElement && xml.name() == "exercise" &&
                xml.attributes().hasAttribute("name") && xml.attributes().value("name") == name)
            readExercise(xml);
        token = xml.readNext();
    }
}

uint Exercise::countExercises()
{
    QFile info(Exercise::character.absoluteFilePath("info.xml"));
    if (!info.open(QFile::ReadOnly | QIODevice::Text))
        return 0;

    QXmlStreamReader xml(&info);
    uint count = 0;
    while (!xml.atEnd())
    {
        QXmlStreamReader::TokenType token = xml.readNext();

        if (token == QXmlStreamReader::StartElement && xml.name() == "exercises" &&
                xml.attributes().hasAttribute("count"))
            return xml.attributes().value("count").toUInt();

        if (token == QXmlStreamReader::StartElement && xml.name() == "exercise")
            count++;
    }

    if (xml.hasError()) return 0;
    return count;
}


void Exercise::setCharacter(const QString &ch)
{
    Exercise::character = QDir(ch);
}

QString Exercise::noExerciseImage()
{
    QFile info(Exercise::character.absoluteFilePath("info.xml"));
    if (!info.open(QFile::ReadOnly | QIODevice::Text))
        return "";

    QXmlStreamReader xml(&info);
    while (!xml.atEnd())
    {
        xml.readNext();
        if (xml.tokenType() == QXmlStreamReader::StartElement &&
                xml.name() == "noExerciseImage")
            return Exercise::character.absoluteFilePath(getData(xml));
    }

    return "";
}
