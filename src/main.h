/*
*   Copyright (c) 2014 Pawel Zimnicki(zimnicky@gmail.com)
*
*   This file is part of the Alopex Eyes.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*   THE SOFTWARE.
*/
#ifndef MAIN_H
#define MAIN_H

#include <QObject>
#include <QMenu>
#include <QSystemTrayIcon>

#include "settings.h"
#include "timer.h"
#include "shorttimer.h"
#include "longtimer.h"
#include "settingsdialog.h"
#include "aboutdialog.h"

class Main : public QObject
{
    Q_OBJECT
public:
    explicit Main(QObject *parent = 0);
    ~Main();
    void start();
signals:

public slots:
    void showSettingsDialog();
    void showAboutDialog();

protected:
    ShortTimer *shortTimer;
    LongTimer *longTimer;
    Settings *settings;
    SettingsDialog *settingsDialog;
    AboutDialog *aboutDialog;

    QSystemTrayIcon *trayIcon;
    bool longTimerEnabled;
    bool shortTimerEnabled;

    void loadSettings();
    void createMenu();
    void retranslateMenu();
};

#endif // MAIN_H
